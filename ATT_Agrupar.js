/*
 * Script para Google Spreadsheet app-script.
 *
 * Usa la configuración de la hoja "groupconfig" para agrupar resultados
 * de google-analytics usando una dimensión.
 *
 * @version 0.2
 * @date    2013-07-22
 * @autor   leo020588@gmail.com
 */

var ATT_Agrupar = {


	// datos agrupados
	data: {},


	// configuración para agrupar
	config: {
		query_id: '',    // id de la configuración en "gaconfig"
		sheet_name: '',  // hoja en la que se guardan los datos de GA
		group_by: '',    // dimensión por la que agrupar
		metrics: [],     // métricas a sumar
		groups: {}       // grupos
	},


	//
	initialize: function () {
		SpreadsheetApp.getActive().addMenu('Grupos', [{name: 'Agrupar', functionName: 'ATT_Agrupar_Run'}]);
	},


	//
	run: function (
		ActiveSpreadsheet
	) {

		this.sps = ActiveSpreadsheet;

		this.readConfig();
		this.buildData();
		this.writeData();
	},


	/*
	 * Carga la configuración de la hoja "groupconfig" y "gaconfig"
	 */
	readConfig: function () {

		// sheet "groupconfig"
		var
		sheet  = this.sps.getSheetByName('groupconfig'),
		values = sheet.getDataRange().getValues();


		// config: query_id
		this.config.query_id = values[0][1];


		// sheet "gaconfig"
		var
		gaconfig_sheet = this.sps.getSheetByName('gaconfig'),
		gaconfig_coord = ATT_Tools.findCellByText(gaconfig_sheet, this.config.query_id),
		gaconfig_range = gaconfig_sheet.getRange(gaconfig_coord.y + 1, gaconfig_coord.x + 1 + 1, gaconfig_sheet.getLastRow(), 1);


		// config: sheet_name
		this.config.sheet_name = gaconfig_range.getCell(14, 1).getValue();


		// config: group_by
		this.config.group_by = values[1][1];


		// config: metrics
		this.config.metrics = gaconfig_range.getCell(7, 1).getValue().split(',');


		// config: groups
		var headers = [], value, y, x;

		for (y = 3; y < values.length; y++) {
			for (x = 0; x < values[y].length; x++) {
				value = values[y][x];

				if (value === '') { continue; }

				// headers (group name)
				if (y === 3) {
					headers[x] = value;
					this.config.groups[headers[x]] = [];

				// values (group items)
				} else {
					this.config.groups[headers[x]].push(value);
				}
			}
		}
	},


	/*
	 * Construye (agrupa y suma) los datos de los grupos
	 */
	buildData: function () {

		var

		// hoja en la que se encuentran los datos de GA
		sheet = this.sps.getSheetByName(this.config.sheet_name),

		// fila en la que comienza los datos de GA
		row_number = 12,

		// guarda la posición de la dimesión y métricas
		indexes = {group_by: 0, metrics: {}},

		// columna en la que acaban los datos de GA
		last_column = ATT_Tools.findFirstEmptyCell(sheet, row_number) - 1,

		// cabeceras de GA (dimensiones y métricas)
		headers = sheet.getRange(row_number++, 1, 1, last_column).getValues(),


		// cargando indexes
		header, x;

		for (x = 0; x < last_column; x++) {
			header = headers[0][x];

			if (header === '') { break; }

			// dimensión (para agrupar)
			if (header === this.config.group_by) {
				indexes.group_by = x;

			// métricas
			} else if (this.config.metrics.indexOf(header) > -1) {
				indexes.metrics[header] = x;
			}
		}


		// agrupando
		var values, value, empty, group, metric;

		while (true) {

			// valores (fila por fila)
			values = sheet.getRange(row_number++, 1, 1, last_column).getValues();

			// controla si toda fila está vacía
			empty  = true;

			// grupo al que partenece
			group  = this.findGroupByItem(values[0][indexes.group_by]);

			//
			if (!group) {
				break;
			}

			//
			if (!this.data[group]) {
				this.data[group] = {};
			}


			// sumando métricas
			for (metric in indexes.metrics) {
				value = values[0][indexes.metrics[metric]];

				if (value === '') { continue; }

				if (!this.data[group][metric]) {
					this.data[group][metric] = value;
				} else {
					this.data[group][metric] += value;
				}

				empty = false;
			}


			// si toda la fina está vacía, no hay más datos
			if (empty) { break; }
		}
	},


	/*
	 * Escribe los datos en la hoja
	 */
	writeData: function () {

		// datos de la tabla [y][x]
		var data_table = [],


		// table-headers
		headers = [this.config.group_by].concat(this.config.metrics);
		data_table.push(headers);


		// table-values
		var row, group, metric;

		for (group in this.data) {
			row = [group];

			for (metric in this.data[group]) {
				row.push(this.data[group][metric]);
			}

			data_table.push(row);
		}


		// escribir al lado de los datos de GA
		var sheet = this.sps.getSheetByName(this.config.sheet_name),

		// fila/columna en la que comienzan los datos
		row_number = 12,
		column_number = ATT_Tools.findFirstEmptyCell(sheet, row_number) + 1,

		// rango de datos
		range = sheet.getRange(row_number, column_number, data_table.length, headers.length).setValues(data_table);


		// mostrar la hoja
		sheet.activate();
		range.activate();
	},


	/*
	 * Busca el grupo al que pertenece el "item"
	 */
	findGroupByItem: function  (
		item
	) {

		var group = undefined, k;

		if (item === '') {
			group = null;

		} else {
			for (k in this.config.groups) {
				if (this.config.groups[k].indexOf(item) > -1) {
					group = k;
					break;
				}
			}

			if (group === undefined) {
				group = '[x] ' + item;
			}
		}

		return group;
	}

};


var ATT_Tools = {

	/*
	 * Busca las coordenadas de una celda que contiene a "text"
	 */
	findCellByText: function (
		sheet,
		text
	) {

		var values = sheet.getDataRange().getValues(), y, x;

		for (y = 0; y < values.length; y++) {
			for (x = 0; x < values[y].length; x++) {
				if (values[y][x] === text) {
					return {'y': y, 'x': x};
				}
			}
		}

		return null;
	},


	/*
	 * Busca la posición de la primera celda vacía en determinada fila
	 */
	findFirstEmptyCell: function (
		sheet,
		row_number
	) {

		var column_number = 0, value;

		while (true) {
			value = sheet.getRange(row_number, ++column_number, 1, 1).getValue();

			if (value === '') {
				return column_number;
			}
		}

		return null;
	}

};


// init
function onOpen () {
	ATT_Agrupar.initialize();
}


// agrupar
function ATT_Agrupar_Run () {
	ATT_Agrupar.run(SpreadsheetApp.getActiveSpreadsheet());
}